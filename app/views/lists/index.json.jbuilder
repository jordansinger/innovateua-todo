json.array!(@lists) do |list|
  json.extract! list, :id, :ttitle
  json.url list_url(list, format: :json)
end
